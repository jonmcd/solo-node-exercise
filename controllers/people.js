const fetch = require('node-fetch');

async function fetchPage(pageNumber, res) {
  const url = `${process.env.SWAPI_URL}/people?page=${pageNumber}`
  const options = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(url, options);
  if (!response.ok) throw new Error(await response.text());
  const json = await response.json();
  return json;
}

async function buildPeopleList(res) {
  const page1 = await fetchPage(1, res);
  const totalPages = Math.ceil(page1.count / 10);
  
  const requests = [];
  for (let pageNum = 2; pageNum <= totalPages; pageNum++) {
    requests.push(fetchPage(pageNum, res))
  }
  const pages = await Promise.all(requests);
  return pages.reduce((acc, page) => [...acc, ...page.results], page1.results)
}

async function getPeople(req, res) {
  const { sortBy } = req.query;
  try {
    const list = await buildPeopleList(res);
  
    if (sortBy && list[0][sortBy] && typeof list[0][sortBy] !== 'object') {
      list.sort((a, b) => {
        if (parseInt(a[sortBy]) && parseInt(b[sortBy])) {
          return a[sortBy].replace(/,/g, "") - b[sortBy].replace(/,/g, "");
        };
        return (a[sortBy].toLowerCase() > b[sortBy].toLowerCase() ? 1 : -1);
      });
    };

    res.status(200).send(list);
  } catch(err) {
    res.status(500).send(err.message);
  }

};

module.exports = {
  getPeople,
  buildPeopleList,
};
