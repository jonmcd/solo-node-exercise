module.exports = {
  people: require('./people'),
  planets: require('./planets'),
};
