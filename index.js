const express = require('express');
const controllers = require('./controllers');

const app = express();

app.use(express.json());

app.get('/people', controllers.people.getPeople);

app.get('/planets', controllers.planets.getPlanets);

app.listen(process.env.SERVER_PORT, () => {
  console.log(`listening on port ${process.env.SERVER_PORT}`);
});
